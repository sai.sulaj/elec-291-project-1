; Talking_Stop_Watch.asm:  The name says it all!
; P2.6 is the START push button
; P3.0 is the STOP push button.  Pressing this button plays the ellapsed time.
; P0.3 is the CLEAR push button.
; The SPI flash memory is assumed to be loaded with 'stop_watch.wav'
; The state diagram of the playback FSM is available as 'Stop_Watch_FSM.pdf'
;
; Copyright (C) 2012-2019  Jesus Calvino-Fraga, jesusc (at) ece.ubc.ca
; 
; This program is free software; you can redistribute it and/or modify it
; under the terms of the GNU General Public License as published by the
; Free Software Foundation; either version 2, or (at your option) any
; later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License
; along with this program; if not, write to the Free Software
; Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
; 
; Connections:
; 
; P89LPC9351  SPI_FLASH
; P2.5        Pin 6 (SPI_CLK)
; P2.2        Pin 5 (MOSI)
; P2.3        Pin 2 (MISO)
; P2.4        Pin 1 (CS/)
; GND         Pin 4
; 3.3V        Pins 3, 7, 8
;
; P0.4 is the DAC output which should be connected to the input of an amplifier (LM386 or similar)
;
; P2.6, P3.0, and P0.3 are connected to push buttons
;
; LCD uses pins P0.5, P0.6, P0.7, P1.2, P1.3, P1.4, P1.6
; WARNING: P1.2 and P1.3 need each a 1k ohm pull-up resistor to VCC (according to the datasheet!).
;
; P2.7 is used (with a transistor) to turn the speaker on/off so it doesn't have a clicking sound.  Use a NPN BJT
; like the 2N3904 or 2N2222A.  The emitter is connected to GND.  The base is connected to a 330 ohm resistor
; and pin P2.7; the other pin of the resistor is connected to 5V.  The collector is connected to the '-'
; terminal of the speaker.
;

$NOLIST
$MOD9351
$LIST

CLK         EQU 14746000  ; Microcontroller system clock frequency in Hz
CCU_RATE    EQU 22050     ; 22050Hz is the sampling rate of the wav file we are playing
CCU_RELOAD  EQU ((65536-((CLK/(2*CCU_RATE)))))
BAUD        EQU 115200
BRVAL       EQU ((CLK/BAUD)-16)

TIMER1_RATE   EQU 200     ; 200Hz, for a timer tick of 5ms
TIMER1_RELOAD EQU ((65536-(CLK/(2*TIMER1_RATE))))

FLASH_CE    EQU P2.4
SOUND       EQU P2.7

; Commands supported by the SPI flash memory according to the datasheet
WRITE_ENABLE     EQU 0x06  ; Address:0 Dummy:0 Num:0
WRITE_DISABLE    EQU 0x04  ; Address:0 Dummy:0 Num:0
READ_STATUS      EQU 0x05  ; Address:0 Dummy:0 Num:1 to infinite
READ_BYTES       EQU 0x03  ; Address:3 Dummy:0 Num:1 to infinite
READ_SILICON_ID  EQU 0xab  ; Address:0 Dummy:3 Num:1 to infinite
FAST_READ        EQU 0x0b  ; Address:3 Dummy:1 Num:1 to infinite
WRITE_STATUS     EQU 0x01  ; Address:0 Dummy:0 Num:1
WRITE_BYTES      EQU 0x02  ; Address:3 Dummy:0 Num:1 to 256
ERASE_ALL        EQU 0xc7  ; Address:0 Dummy:0 Num:0
ERASE_BLOCK      EQU 0xd8  ; Address:3 Dummy:0 Num:0
READ_DEVICE_ID   EQU 0x9f  ; Address:0 Dummy:2 Num:1 to infinite

dseg at 30H
minute:       ds 1
w:             ds 3 ; 24-bit play counter.  Decremented in CCU ISR.
seconds:       ds 1
;r5:  ds 1 ; seconds running on the state

T2S_FSM_state: ds 1
Count5ms:      ds 1

active_profile: ds 1
soaktemp_p1: ds 2
soaktime_p1: ds 1
reflowtemp_p1: ds 2
reflowtime_p1: ds 1
curr_temp: ds 2
curr_state:    ds 1 ; state counter
;soaktemp_p2: ds 2
;soaktime_p2: ds 1
;reflowtemp_p2: ds 2
;reflowtime_p2: ds 1
;curr_temp: ds 2
;soaktemp_p3: ds 2
;soaktime_p3: ds 1
;reflowtemp_p3: ds 2
;reflowtime_p3: ds 1

BSEG
T2S_FSM_start: dbit 1
seconds_flag:  dbit 1
start_time: dbit 1
display_mode: dbit 1 ; 0 -> Status, 1 -> Profile

; Connect pushbuttons to this pins to, start, stop, or clear the stop watch
START equ P2.6
STOP  equ P3.0
CLEAR equ P0.3
DEBUG equ P2.1

cseg

org 0x0000 ; Reset vector
    ljmp MainProgram

org 0x001B ; Timer/Counter 1 overflow interrupt vector
	ljmp Timer1_ISR

org 0x005b ; CCU interrupt vector.  Used in this code to replay the wave file.
	ljmp CCU_ISR

cseg
; These 'equ' must match the wiring between the microcontroller and the LCD!
LCD_RS equ P0.5
LCD_RW equ P0.6
LCD_E  equ P0.7
LCD_D4 equ P1.2
LCD_D5 equ P1.3
LCD_D6 equ P1.4
LCD_D7 equ P1.6
$NOLIST
$include(LCD_4bit_LPC9351.inc) ; A library of LCD related functions and utility macros
$LIST


DSEG at 0x30
x:   ds 4
y:   ds 4
bcd: ds 5


BSEG
mf: dbit 1

$NOLIST
$INCLUDE(math32.inc)
$LIST

;---------------------------------;
; Routine to initialize the ISR   ;
; for timer 1                     ;
;---------------------------------;
Timer1_Init:
	mov a, TMOD
	anl a, #0x0f ; Clear the bits for timer 1
	orl a, #0x10 ; Configure timer 1 as 16-timer
	mov TMOD, a
	mov TH1, #high(TIMER1_RELOAD)
	mov TL1, #low(TIMER1_RELOAD)
	; Enable the timer and interrupts
	setb ET1  ; Enable timer 1 interrupt
	setb TR1  ; Start timer 1
	ret

;---------------------------------;
; ISR for timer 1                 ;
;---------------------------------;
Timer1_ISR:
	mov TH1, #high(TIMER1_RELOAD)
	mov TL1, #low(TIMER1_RELOAD)
	
	; The two registers used in the ISR must be saved in the stack
	push acc
	push psw
	
	; Increment the 8-bit 5-mili-second counter
	inc Count5ms
	lcall Detect_Keypress

Inc_Done:
	; Check if half second has passed
	mov a, Count5ms
	cjne a, #200, Timer1_ISR_done ; Warning: this instruction changes the carry flag!
	
	; 1000 milliseconds have passed.  Set a flag so the main program knows
	setb seconds_flag ; Let the main program know half second had passed
	; Reset to zero the 5-milli-seconds counter, it is a 8-bit variable
	mov Count5ms, #0
	; Increment minutes and seconds
	lcall Render
	jnb start_time, Timer1_ISR_done
	inc seconds
	inc r5 ; the seconds on the state
	mov a, seconds
	cjne a, #60, Timer1_ISR_done
	mov seconds, #0
	inc minute
	mov a, minute
	cjne a, #60, Timer1_ISR_done
	mov minute, #0
Timer1_ISR_done:
	pop psw
	pop acc
	reti

;---------------------------------;
; Routine to initialize the CCU.  ;
; We are using the CCU timer in a ;
; manner similar to the timer 2   ;
; available in other 8051s        ;
;---------------------------------;
CCU_Init:
	mov TH2, #high(CCU_RELOAD)
	mov TL2, #low(CCU_RELOAD)
	mov TOR2H, #high(CCU_RELOAD)
	mov TOR2L, #low(CCU_RELOAD)
	mov TCR21, #10000000b ; Latch the reload value
	mov TICR2, #10000000b ; Enable CCU Timer Overflow Interrupt
	setb ECCU ; Enable CCU interrupt
	setb TMOD20 ; Start CCU timer
	ret

;---------------------------------;
; ISR for CCU.  Used to playback  ;
; the WAV file stored in the SPI  ;
; flash memory.                   ;
;---------------------------------;
CCU_ISR:
	mov TIFR2, #0 ; Clear CCU Timer Overflow Interrupt Flag bit. Actually, it clears all the bits!
	
	; The registers used in the ISR must be saved in the stack
	push acc
	push psw
	
	; Check if the play counter is zero.  If so, stop playing sound.
	mov a, w+0
	orl a, w+1
	orl a, w+2
	jz stop_playing
	
	; Decrement play counter 'w'.  In this implementation 'w' is a 24-bit counter.
	mov a, #0xff
	dec w+0
	cjne a, w+0, keep_playing
	dec w+1
	cjne a, w+1, keep_playing
	dec w+2
	
keep_playing:

	lcall Send_SPI ; Read the next byte from the SPI Flash...
	mov AD1DAT3, a ; and send it to the DAC
	
	sjmp CCU_ISR_Done

stop_playing:
	clr TMOD20 ; Stop CCU timer
	setb FLASH_CE  ; Disable SPI Flash
	clr SOUND ; Turn speaker off

CCU_ISR_Done:	
	pop psw
	pop acc
	reti
	
Send_BCD mac
push ar0
mov r0, %0
lcall ?Send_BCD
pop ar0
endmac

?Send_BCD:push acc; Write most significant digit
mov a, r0
swap a
anl a, #0fh
orl a, #30h

lcall putchar; write least significant digit
mov a, r0
anl a, #0fh
orl a, #30h
lcall putchar
pop acc
ret

;---------------------------------;
; Initial configuration of ports. ;
; After reset the default for the ;
; pins is 'Open Drain'.  This     ;
; routine changes them pins to    ;
; Quasi-bidirectional like in the ;
; original 8051.                  ;
; Notice that P1.2 and P1.3 are   ;
; always 'Open Drain'. If those   ;
; pins are to be used as output   ;
; they need a pull-up resistor.   ;
;---------------------------------;
Ports_Init:
	; Configure all the ports in bidirectional mode:
	mov P0M1, #00H
	mov P0M2, #00H
	mov P1M1, #00H
	mov P1M2, #00H ; WARNING: P1.2 and P1.3 need 1 kohm pull-up resistors if used as outputs!
	mov P2M1, #00H
	mov P2M2, #00H
	mov P3M1, #00H
	mov P3M2, #00H
	ret

;---------------------------------;
; Initialize ADC1/DAC1 as DAC1.   ;
; Warning, the ADC1/DAC1 can work ;
; only as ADC or DAC, not both.   ;
; The P89LPC9351 has two ADC/DAC  ;
; interfaces.  One can be used as ;
; ADC and the other can be used   ;
; as DAC.  Also configures the    ;
; pin associated with the DAC, in ;
; this case P0.4 as 'Open Drain'. ;
;---------------------------------;
; TTL Might need to move this
InitDAC1:
	; Configure pin P0.4 (DAC1 output pin) as open drain
	orl	P0M1,   #00010000B
	orl	P0M2,   #00010000B
	mov ADMODB, #00101000B ; Select main clock/2 for ADC/DAC.  Also enable DAC1 output (Table 25 of reference manual)
	mov	ADCON1, #00000100B ; Enable the converter
	mov AD1DAT3, #0x80     ; Start value is 3.3V/2 (zero reference for AC WAV file)
	ret

;---------------------------------;
; Initialize ADC0/DAC0 as ADC0.   ;
;---------------------------------;
InitADC0:
	; ADC0_0 is connected to P1.7
	; ADC0_1 is connected to P0.0
	; ADC0_2 is connected to P2.1
	; ADC0_3 is connected to P2.0
	; Configure pins P1.7, P0.0, P2.1, and P2.0 as inputs
	orl P0M1, #00000001b
	anl P0M2, #11111110b
	orl P1M1, #10000000b
	anl P1M2, #01111111b
	; orl P2M1, #00000011b
	; anl P2M2, #11111100b
	; Setup ADC0
	setb BURST0 ; Autoscan continuos conversion mode
	mov	ADMODB,#0x20 ;ADC0 clock is 7.3728MHz/2
	mov	ADINS,#00000011b ; Select the four channels of ADC0 for conversion
	mov	ADCON0,#0x05 ; Enable the converter and start immediately
	; Wait for first conversion to complete
InitADC0_L1:
	mov	a,ADCON0
	jnb	acc.3,InitADC0_L1
	ret

;---------------------------------;
; Change the internal RC osc. clk ;
; from 7.373MHz to 14.746MHz.     ;
;---------------------------------;
Double_Clk:
    mov dptr, #CLKCON
    movx a, @dptr
    orl a, #00001000B ; double the clock speed to 14.746MHz
    movx @dptr,a
	ret

;---------------------------------;
; Sends a byte via serial port    ;
;---------------------------------;
putchar:
	jbc	TI,putchar_L1
	sjmp putchar
putchar_L1:
	mov	SBUF,a
	ret

;---------------------------------;
; Receive a byte from serial port ;
;---------------------------------;
getchar:
	jbc	RI,getchar_L1
	sjmp getchar
getchar_L1:
	mov	a,SBUF
	ret

;---------------------------------;
; Initialize the serial port      ;
;---------------------------------;
InitSerialPort:
	mov	BRGCON,#0x00
	mov	BRGR1,#high(BRVAL)
	mov	BRGR0,#low(BRVAL)
	mov	BRGCON,#0x03 ; Turn-on the baud rate generator
	mov	SCON,#0x52 ; Serial port in mode 1, ren, txrdy, rxempty
	; Make sure that TXD(P1.0) and RXD(P1.1) are configured as bidrectional I/O
	anl	P1M1,#11111100B
	anl	P1M2,#11111100B
	ret
;---------------------------------;
; Initialize the SPI interface    ;
; and the pins associated to SPI. ;
;---------------------------------;
Init_SPI:
	; Configure MOSI (P2.2), CS* (P2.4), and SPICLK (P2.5) as push-pull outputs (see table 42, page 51)
	anl P2M1, #low(not(00110100B))
	orl P2M2, #00110100B
	; Configure MISO (P2.3) as input (see table 42, page 51)
	orl P2M1, #00001000B
	anl P2M2, #low(not(00001000B)) 
	; Configure SPI
	mov SPCTL, #11010000B ; Ignore /SS, Enable SPI, DORD=0, Master=1, CPOL=0, CPHA=0, clk/4
	ret

;---------------------------------;
; Sends AND receives a byte via   ;
; SPI.                            ;
;---------------------------------;
Send_SPI:
	mov SPDAT, a
Send_SPI_1:
	mov a, SPSTAT 
	jnb acc.7, Send_SPI_1 ; Check SPI Transfer Completion Flag
	mov SPSTAT, a ; Clear SPI Transfer Completion Flag
	mov a, SPDAT ; return received byte via accumulator
	ret

;---------------------------------;
; SPI flash 'write enable'        ;
; instruction.                    ;
;---------------------------------;
Enable_Write:
	clr FLASH_CE
	mov a, #WRITE_ENABLE
	lcall Send_SPI
	setb FLASH_CE
	ret

;---------------------------------;
; This function checks the 'write ;
; in progress' bit of the SPI     ;
; flash memory.                   ;
;---------------------------------;
Check_WIP:
	clr FLASH_CE
	mov a, #READ_STATUS
	lcall Send_SPI
	mov a, #0x55
	lcall Send_SPI
	setb FLASH_CE
	jb acc.0, Check_WIP ;  Check the Write in Progress bit
	ret

	
;---------------------------------;
; CRC-CCITT (XModem) Polynomial:  ;
; x^16 + x^12 + x^5 + 1 (0x1021)  ;
; CRC in [R7,R6].                 ;
; Converted to a macro to remove  ;
; the overhead of 'lcall' and     ;
; 'ret' instructions, since this  ;
; 'routine' may be executed over  ;
; 4 million times!                ;
;---------------------------------;
;crc16:
crc16 mac
	xrl	a, r7			; XOR high of CRC with byte
	mov r0, a			; Save for later use
	mov	dptr, #CRC16_TH ; dptr points to table high
	movc a, @a+dptr		; Get high part from table
	xrl	a, r6			; XOR With low byte of CRC
	mov	r7, a			; Store to high byte of CRC
	mov a, r0			; Retrieve saved accumulator
	mov	dptr, #CRC16_TL	; dptr points to table low	
	movc a, @a+dptr		; Get Low from table
	mov	r6, a			; Store to low byte of CRC
	;ret
endmac

;---------------------------------;
; High constants for CRC-CCITT    ;
; (XModem) Polynomial:            ;
; x^16 + x^12 + x^5 + 1 (0x1021)  ;
;---------------------------------;
CRC16_TH:
	db	000h, 010h, 020h, 030h, 040h, 050h, 060h, 070h
	db	081h, 091h, 0A1h, 0B1h, 0C1h, 0D1h, 0E1h, 0F1h
	db	012h, 002h, 032h, 022h, 052h, 042h, 072h, 062h
	db	093h, 083h, 0B3h, 0A3h, 0D3h, 0C3h, 0F3h, 0E3h
	db	024h, 034h, 004h, 014h, 064h, 074h, 044h, 054h
	db	0A5h, 0B5h, 085h, 095h, 0E5h, 0F5h, 0C5h, 0D5h
	db	036h, 026h, 016h, 006h, 076h, 066h, 056h, 046h
	db	0B7h, 0A7h, 097h, 087h, 0F7h, 0E7h, 0D7h, 0C7h
	db	048h, 058h, 068h, 078h, 008h, 018h, 028h, 038h
	db	0C9h, 0D9h, 0E9h, 0F9h, 089h, 099h, 0A9h, 0B9h
	db	05Ah, 04Ah, 07Ah, 06Ah, 01Ah, 00Ah, 03Ah, 02Ah
	db	0DBh, 0CBh, 0FBh, 0EBh, 09Bh, 08Bh, 0BBh, 0ABh
	db	06Ch, 07Ch, 04Ch, 05Ch, 02Ch, 03Ch, 00Ch, 01Ch
	db	0EDh, 0FDh, 0CDh, 0DDh, 0ADh, 0BDh, 08Dh, 09Dh
	db	07Eh, 06Eh, 05Eh, 04Eh, 03Eh, 02Eh, 01Eh, 00Eh
	db	0FFh, 0EFh, 0DFh, 0CFh, 0BFh, 0AFh, 09Fh, 08Fh
	db	091h, 081h, 0B1h, 0A1h, 0D1h, 0C1h, 0F1h, 0E1h
	db	010h, 000h, 030h, 020h, 050h, 040h, 070h, 060h
	db	083h, 093h, 0A3h, 0B3h, 0C3h, 0D3h, 0E3h, 0F3h
	db	002h, 012h, 022h, 032h, 042h, 052h, 062h, 072h
	db	0B5h, 0A5h, 095h, 085h, 0F5h, 0E5h, 0D5h, 0C5h
	db	034h, 024h, 014h, 004h, 074h, 064h, 054h, 044h
	db	0A7h, 0B7h, 087h, 097h, 0E7h, 0F7h, 0C7h, 0D7h
	db	026h, 036h, 006h, 016h, 066h, 076h, 046h, 056h
	db	0D9h, 0C9h, 0F9h, 0E9h, 099h, 089h, 0B9h, 0A9h
	db	058h, 048h, 078h, 068h, 018h, 008h, 038h, 028h
	db	0CBh, 0DBh, 0EBh, 0FBh, 08Bh, 09Bh, 0ABh, 0BBh
	db	04Ah, 05Ah, 06Ah, 07Ah, 00Ah, 01Ah, 02Ah, 03Ah
	db	0FDh, 0EDh, 0DDh, 0CDh, 0BDh, 0ADh, 09Dh, 08Dh
	db	07Ch, 06Ch, 05Ch, 04Ch, 03Ch, 02Ch, 01Ch, 00Ch
	db	0EFh, 0FFh, 0CFh, 0DFh, 0AFh, 0BFh, 08Fh, 09Fh
	db	06Eh, 07Eh, 04Eh, 05Eh, 02Eh, 03Eh, 00Eh, 01Eh

;---------------------------------;
; Low constants for CRC-CCITT     ;
; (XModem) Polynomial:            ;
; x^16 + x^12 + x^5 + 1 (0x1021)  ;
;---------------------------------;
CRC16_TL:
	db	000h, 021h, 042h, 063h, 084h, 0A5h, 0C6h, 0E7h
	db	008h, 029h, 04Ah, 06Bh, 08Ch, 0ADh, 0CEh, 0EFh
	db	031h, 010h, 073h, 052h, 0B5h, 094h, 0F7h, 0D6h
	db	039h, 018h, 07Bh, 05Ah, 0BDh, 09Ch, 0FFh, 0DEh
	db	062h, 043h, 020h, 001h, 0E6h, 0C7h, 0A4h, 085h
	db	06Ah, 04Bh, 028h, 009h, 0EEh, 0CFh, 0ACh, 08Dh
	db	053h, 072h, 011h, 030h, 0D7h, 0F6h, 095h, 0B4h
	db	05Bh, 07Ah, 019h, 038h, 0DFh, 0FEh, 09Dh, 0BCh
	db	0C4h, 0E5h, 086h, 0A7h, 040h, 061h, 002h, 023h
	db	0CCh, 0EDh, 08Eh, 0AFh, 048h, 069h, 00Ah, 02Bh
	db	0F5h, 0D4h, 0B7h, 096h, 071h, 050h, 033h, 012h
	db	0FDh, 0DCh, 0BFh, 09Eh, 079h, 058h, 03Bh, 01Ah
	db	0A6h, 087h, 0E4h, 0C5h, 022h, 003h, 060h, 041h
	db	0AEh, 08Fh, 0ECh, 0CDh, 02Ah, 00Bh, 068h, 049h
	db	097h, 0B6h, 0D5h, 0F4h, 013h, 032h, 051h, 070h
	db	09Fh, 0BEh, 0DDh, 0FCh, 01Bh, 03Ah, 059h, 078h
	db	088h, 0A9h, 0CAh, 0EBh, 00Ch, 02Dh, 04Eh, 06Fh
	db	080h, 0A1h, 0C2h, 0E3h, 004h, 025h, 046h, 067h
	db	0B9h, 098h, 0FBh, 0DAh, 03Dh, 01Ch, 07Fh, 05Eh
	db	0B1h, 090h, 0F3h, 0D2h, 035h, 014h, 077h, 056h
	db	0EAh, 0CBh, 0A8h, 089h, 06Eh, 04Fh, 02Ch, 00Dh
	db	0E2h, 0C3h, 0A0h, 081h, 066h, 047h, 024h, 005h
	db	0DBh, 0FAh, 099h, 0B8h, 05Fh, 07Eh, 01Dh, 03Ch
	db	0D3h, 0F2h, 091h, 0B0h, 057h, 076h, 015h, 034h
	db	04Ch, 06Dh, 00Eh, 02Fh, 0C8h, 0E9h, 08Ah, 0ABh
	db	044h, 065h, 006h, 027h, 0C0h, 0E1h, 082h, 0A3h
	db	07Dh, 05Ch, 03Fh, 01Eh, 0F9h, 0D8h, 0BBh, 09Ah
	db	075h, 054h, 037h, 016h, 0F1h, 0D0h, 0B3h, 092h
	db	02Eh, 00Fh, 06Ch, 04Dh, 0AAh, 08Bh, 0E8h, 0C9h
	db	026h, 007h, 064h, 045h, 0A2h, 083h, 0E0h, 0C1h
	db	01Fh, 03Eh, 05Dh, 07Ch, 09Bh, 0BAh, 0D9h, 0F8h
	db	017h, 036h, 055h, 074h, 093h, 0B2h, 0D1h, 0F0h

; Display a 3-digit BCD number in the LCD
LCD_3BCD:
; I removed this to only diplay 3 digits rather than 4?
	; mov a, bcd+1
	; swap a
	; anl a, #0x0f
	; orl a, #'0'
	; lcall ?WriteData
	mov a, bcd+1
	anl a, #0x0f
	orl a, #'0'
	lcall ?WriteData
	mov a, bcd+0
	swap a
	anl a, #0x0f
	orl a, #'0'
	lcall ?WriteData
	mov a, bcd+0
	anl a, #0x0f
	orl a, #'0'
	lcall ?WriteData
	ret
	
Display_ADC_Values:
	push x
	push y
	mov x+0, AD0DAT1
	mov x+1, #0
	mov x+2, #0
	mov x+3, #0
	Load_y(3267)
	lcall mul32
	Load_y(255)
	lcall div32
	Load_y(50)
	lcall mul32
	Load_y(533)
	lcall div32
	Load_y(20)
	lcall add32
	lcall Hex2BCD
	lcall Send_Putty
	Set_Cursor(2, 7)
	lcall LCD_3BCD
	lcall State_Machine
	pop y
	pop x
	ret
	
Send_Putty:
	mov a, #'T' 
    lcall putchar
    mov a, #':'  
    lcall putchar
	;Send_BCD(bcd+3)
    ;Send_BCD(bcd+2)
    Send_BCD(bcd+1)
    Send_BCD(bcd+0)
    mov a, #'\n'  
    lcall putchar
    mov a, #'\r' ; move cursor all the way to the left
    lcall putchar
	mov a, #'S' 
    lcall putchar
    mov a, #':'  
    lcall putchar
    Send_BCD(curr_state)
    mov a, #'\n'  
    lcall putchar
    mov a, #'\r' ; move cursor all the way to the left
    lcall putchar
    ret
    
State_Machine:
	mov a,curr_state
State0:
	cjne a,#0,State1
	jnb start_time, SSR_off
	clr P3.1
	inc curr_state
State1: ;; soak temp
	cjne a,#1,State2
	Load_y(150)
	lcall x_lt_y
	jb mf, Same_State
 	inc curr_state
 	mov r5,#0
 	ljmp SSR_off
State2: ;; soak time
	cjne a,#2,State3
	mov a,r5
	cjne a,#60,Same_State
	inc curr_state
	mov r5,#0
	clr P3.1
State3: ;; reflow temp
	cjne a,#3,State4
	Load_y(220)
	lcall x_lt_y
	jb mf, Same_State
	mov r5,#0
	inc curr_state
	ljmp SSR_off
State4: ;; reflow time
	cjne a,#4,State5
	mov a,r5
	cjne a,#45,Same_State
	inc curr_state
	mov r5,#0
State5: ;; cool down 
	cjne a,#5,Same_State
	mov a,r5
	cjne a,#20,Same_State
	lcall Reset
SSR_off:
	setb P3.1
Same_State:
	ret 
	
clear_time:
	clr a
	mov r5,a
	cpl debug
	ret

; Moves relevant soak temp depending
; on active_profile to x
Get_Soak_Temp:
	push acc
	mov a, active_profile
	mov x+2, #0
	mov x+3, #0
	cjne a, #3, Get_Soak_Temp_P2
	;mov x+0, soaktemp_p3+0
	;mov x+1, soaktemp_p3+1
	ljmp Get_Soak_Temp_Done
Get_Soak_Temp_P2:
	cjne a, #2, Get_Soak_Temp_P1
	;mov x+0, soaktemp_p2+0
	;mov x+1, soaktemp_p2+1
	ljmp Get_Soak_Temp_Done
Get_Soak_Temp_P1:
	mov x+0, soaktemp_p1+0
	mov x+1, soaktemp_p1+1
Get_Soak_Temp_Done:
	pop acc
	ret

; Moves relevant soak time depending
; on active_profile to a
Get_Soak_Time:
	mov a, active_profile
	cjne a, #3, Get_Soak_Time_P2
	;mov a, soaktime_p3
	ljmp Get_Soak_Time_Done
Get_Soak_Time_P2:
	cjne a, #2, Get_Soak_Time_P1
;	mov a, soaktime_p2
	ljmp Get_Soak_Time_Done
Get_Soak_Time_P1:
	mov a, soaktime_p1
Get_Soak_Time_Done:
	ret

; Moves relevant reflow temp depending
; on active_profile to x
Get_Reflow_Temp:
	push acc
	mov a, active_profile
	mov x+2, #0
	mov x+3, #0
	cjne a, #3, Get_Reflow_Temp_P2
	;mov x+0, reflowtemp_p3+0
	;mov x+1, reflowtemp_p3+1
	ljmp Get_Reflow_Temp_Done
Get_Reflow_Temp_P2:
	cjne a, #2, Get_Reflow_Temp_P1
;	mov x+0, reflowtemp_p2+0
;	mov x+1, reflowtemp_p2+1
	ljmp Get_Reflow_Temp_Done
Get_Reflow_Temp_P1:
	mov x+0, reflowtemp_p1+0
	mov x+1, reflowtemp_p1+1
Get_Reflow_Temp_Done:
	pop acc
	ret

; Moves relevant reflow time depending
; on active_profile to x
Get_Reflow_Time:
	mov a, active_profile
	cjne a, #3, Get_Reflow_Time_P2
	;mov a, reflowtime_p3
	ljmp Get_Reflow_Time_Done
Get_Reflow_Time_P2:
	cjne a, #2, Get_Reflow_Time_P1
;	mov a, reflowtime_p2
	ljmp Get_Reflow_Time_Done
Get_Reflow_Time_P1:
	mov a, reflowtime_p1
Get_Reflow_Time_Done:
	ret
	
; Display a binary number in the LCD (must be less than 99).  Number to display passed in accumulator.
LCD_number:
	push acc
	mov b, #10
	div ab
	orl a, #'0'
	lcall ?WriteData
	mov a, b
	orl a, #'0'
	lcall ?WriteData
	pop acc
	ret

; Sounds we need in the SPI flash: 0; 1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 12; 13; 14; 15; 16; 17; 18; 19; 20; 30; 40; 50; minutes; seconds;
; Approximate index of sounds in file 'stop_watch.wav'
; This was generated using: computer_sender -Asw_index.asm -S2000 stop_watch.wav
sound_index:
    db 0x00, 0x00, 0x2d ; 0 
    db 0x00, 0x31, 0x07 ; 1 
    db 0x00, 0x70, 0x07 ; 2 
    db 0x00, 0xad, 0xb9 ; 3 
    db 0x00, 0xf2, 0x66 ; 4 
    db 0x01, 0x35, 0xd5 ; 5 
    db 0x01, 0x7d, 0x33 ; 6 
    db 0x01, 0xc7, 0x61 ; 7 
    db 0x02, 0x12, 0x79 ; 8 
    db 0x02, 0x49, 0xc1 ; 9 
    db 0x02, 0x8f, 0x7a ; 10 
    db 0x02, 0xd0, 0x63 ; 11 
    db 0x03, 0x1b, 0x87 ; 12 
    db 0x03, 0x63, 0x0e ; 13 
    db 0x03, 0xb9, 0x5f ; 14 
    db 0x04, 0x11, 0x3a ; 15 
    db 0x04, 0x66, 0xc4 ; 16 
    db 0x04, 0xc0, 0x12 ; 17 
    db 0x05, 0x26, 0x98 ; 18 
    db 0x05, 0x74, 0xe9 ; 19 
    db 0x05, 0xd2, 0x8e ; 20 
    db 0x06, 0x1d, 0x83 ; 21 30
    db 0x06, 0x65, 0x46 ; 22 40
    db 0x06, 0xac, 0x69 ; 23 50
    db 0x06, 0xf5, 0x87 ; 24 60
    db 0x07, 0x4c, 0xe9 ; 25 70
    db 0x07, 0xa4, 0xb6 ; 26 80
    db 0x07, 0xe3, 0x11 ; 27 90
    db 0x08, 0x30, 0xeb ; 28 100
    db 0x08, 0x7b, 0x57 ; 29 state
    db 0x08, 0xc7, 0x92 ; 30  temperature
    db 0x09, 0x09, 0x7c ; 31 degreees celisus 

; Size of each sound in 'sound_index'
; Generated using: computer_sender -Asw_index.asm -S2000 stop_watch.wav
Size_Length:
    db 0x00, 0x30, 0xda ; 0 
    db 0x00, 0x3f, 0x00 ; 1 
    db 0x00, 0x3d, 0xb2 ; 2 
    db 0x00, 0x44, 0xad ; 3 
    db 0x00, 0x43, 0x6f ; 4 
    db 0x00, 0x47, 0x5e ; 5 
    db 0x00, 0x4a, 0x2e ; 6 
    db 0x00, 0x4b, 0x18 ; 7 
    db 0x00, 0x37, 0x48 ; 8 
    db 0x00, 0x45, 0xb9 ; 9 
    db 0x00, 0x40, 0xe9 ; 10 
    db 0x00, 0x4b, 0x24 ; 11 
    db 0x00, 0x47, 0x87 ; 12 
    db 0x00, 0x56, 0x51 ; 13 
    db 0x00, 0x57, 0xdb ; 14 
    db 0x00, 0x55, 0x8a ; 15 
    db 0x00, 0x59, 0x4e ; 16 
    db 0x00, 0x66, 0x86 ; 17 
    db 0x00, 0x4e, 0x51 ; 18 
    db 0x00, 0x5d, 0xa5 ; 19 
    db 0x00, 0x4a, 0xf5 ; 20 
    db 0x00, 0x47, 0xc3 ; 21 
    db 0x00, 0x47, 0x23 ; 22 
    db 0x00, 0x49, 0x1e ; 23 
    db 0x00, 0x57, 0x62 ; 24 
    db 0x00, 0x57, 0xcd ; 25 
    db 0x00, 0x3e, 0x5b ; 26 
    db 0x00, 0x4d, 0xda ; 27 
    db 0x00, 0x4a, 0x6c ; 28 
    db 0x00, 0x4c, 0x3b ; 29 
    db 0x00, 0x41, 0xea ; 30 

; The sound and its length from the two tables above is passed in the accumulator.
Play_Sound_Using_Index:
	setb SOUND ; Turn speaker on
	clr TMOD20 ; Stop the CCU from playing previous request
	setb FLASH_CE
	
	; There are three bytes per row in our tables, so multiply index by three
	mov b, #3
	mul ab
	mov R0, a ; Make a copy of the index*3
	
	clr FLASH_CE ; Enable SPI Flash
	mov a, #READ_BYTES
	lcall Send_SPI
	; Set the initial position in memory of where to start playing
	mov dptr, #sound_index
	mov a, R0
	movc a, @a+dptr
	lcall Send_SPI
	inc dptr
	mov a, R0
	movc a, @a+dptr
	lcall Send_SPI
	inc dptr
	mov a, R0
	movc a, @a+dptr
	lcall Send_SPI
	; Now set how many bytes to play
	mov dptr, #Size_Length
	mov a, R0
	movc a, @a+dptr
	mov w+2, a
	inc dptr
	mov a, R0
	movc a, @a+dptr
	mov w+1, a
	inc dptr
	mov a, R0
	movc a, @a+dptr
	mov w+0, a
	
	mov a, #0x00 ; Request first byte to send to DAC
	lcall Send_SPI
	
	setb TMOD20 ; Start playback by enabling CCU timer

	ret

;---------------------------------------------------------------------------------;
; This is the FSM that plays minutes and seconds after the STOP button is pressed ;
; The state diagram of this FSM is available as 'Stop_Watch_FSM.pdf'              ;
;---------------------------------------------------------------------------------;
T2S_FSM:
	mov a, T2S_FSM_state

T2S_FSM_State0: ; Checks for the start signal (T2S_FSM_Start==1)
	cjne a, #0, T2S_FSM_State1
	jnb T2S_FSM_Start, T2S_FSM_State0_Done
	; Check if minutes is larger than 19
	clr c
	mov a, minute
	subb a, #20
	jnc minutes_gt_19
	mov T2S_FSM_state, #1
	sjmp T2S_FSM_State0_Done
minutes_gt_19:
	mov T2S_FSM_state, #3
T2S_FSM_State0_Done:
	ret
	
T2S_FSM_State1: ; Plays minutes when minutes is less than 20
	cjne a, #1, T2S_FSM_State2
	mov a, minute
	lcall Play_Sound_Using_Index
	mov T2S_FSM_State, #2
	ret 

T2S_FSM_State2: ; Stay in this state until sound finishes playing
	cjne a, #2, T2S_FSM_State3
	jb TMOD20, T2S_FSM_State2_Done 
	mov T2S_FSM_State, #6
T2S_FSM_State2_Done:
	ret

T2S_FSM_State3: ; Plays the tens when minutes is larger than 19, for example for 42 minutes, it plays 'forty'
	cjne a, #3, T2S_FSM_State4
	mov a, minute
	mov b, #10
	div ab
	add a, #18
	lcall Play_Sound_Using_Index
	mov T2S_FSM_State, #4
	ret

T2S_FSM_State4: ; Stay in this state until sound finishes playing
	cjne a, #4, T2S_FSM_State5
	jb TMOD20, T2S_FSM_State4_Done 
	mov T2S_FSM_State, #5
T2S_FSM_State4_Done:
    ret

T2S_FSM_State5: ; Plays the units when minutes is larger than 19, for example for 42 minutes, it plays 'two'
	cjne a, #5, T2S_FSM_State6
	mov a, minute
	mov b, #10
	div ab
	mov a, b
	jz T2S_FSM_State5_Done ; Prevents from playing something like 'forty zero'
	lcall Play_Sound_Using_Index
T2S_FSM_State5_Done:
	mov T2S_FSM_State, #2
	ret

T2S_FSM_State6: ; Plays the word 'minutes'
	cjne a, #6, T2S_FSM_State7
	mov a, #24 ; Index 24 has the word 'minutes'
	lcall Play_Sound_Using_Index
	mov T2S_FSM_State, #7
	ret

T2S_FSM_State7: ; Stay in this state until sound finishes playing
	cjne a, #7, T2S_FSM_State8
	jb TMOD20, T2S_FSM_State7_Done 
	; Done playing previous sound, check if seconds is larger than 19
	clr c
	mov a, seconds
	subb a, #20
	jnc seconds_gt_19
	mov T2S_FSM_state, #8
	sjmp T2S_FSM_State0_Done
seconds_gt_19:
	mov T2S_FSM_state, #10
T2S_FSM_State7_Done:
    ret

T2S_FSM_State8: ; Play the seconds when seconds is less than 20.
	cjne a, #8, T2S_FSM_State9
	mov a, seconds
	lcall Play_Sound_Using_Index
	mov T2S_FSM_state, #9
	ret

T2S_FSM_State9: ; Stay in this state until sound finishes playing
	cjne a, #9, T2S_FSM_State10
	jb TMOD20, T2S_FSM_State9_Done 
	mov T2S_FSM_State, #13
T2S_FSM_State9_Done:
	ret

T2S_FSM_State10:  ; Plays the tens when seconds is larger than 19, for example for 35 seconds, it plays 'thirty'
	cjne a, #10, T2S_FSM_State11
	mov a, seconds
	mov b, #10
	div ab
	add a, #18
	lcall Play_Sound_Using_Index
	mov T2S_FSM_state, #11
	ret

T2S_FSM_State11: ; Stay in this state until sound finishes playing
	cjne a, #11, T2S_FSM_State12
	jb TMOD20, T2S_FSM_State11_Done 
	mov T2S_FSM_State, #12
T2S_FSM_State11_Done:
	ret

T2S_FSM_State12: ; Plays the units when seconds is larger than 19, for example for 35 seconds, it plays 'five'
	cjne a, #12, T2S_FSM_State13
	mov a, seconds
	mov b, #10
	div ab
	mov a, b
	jz T2S_FSM_State12_Done ; Prevents from saying something like 'thirty zero'
	lcall Play_Sound_Using_Index
T2S_FSM_State12_Done:
	mov T2S_FSM_State, #9
	ret

T2S_FSM_State13: ; Plays the word 'seconds'
	cjne a, #13, T2S_FSM_State14
	mov a, #25 ; Index 25 has the word 'seconds'
	lcall Play_Sound_Using_Index
	mov T2S_FSM_State, #14
	ret

T2S_FSM_State14: ; Stay in this state until sound finishes playing
	cjne a, #14, T2S_FSM_Error
	jb TMOD20, T2S_FSM_State14_Done 
	clr T2S_FSM_Start 
	mov T2S_FSM_State, #0
T2S_FSM_State14_Done:
	ret

T2S_FSM_Error: ; If we got to this point, there is an error in the finite state machine.  Restart it.
	mov T2S_FSM_state, #0
	clr T2S_FSM_Start
	ret
; End of FMS that plays minutes and seconds

Handle_Key7_Press:
	ret
Handle_Key6_Press:
	ret
Handle_Key5_Press:
	ret
Handle_Key4_Press:
	lcall Reset
	ret
Handle_Key3_Press:
	mov a, active_profile
	cjne a, #4, Handle_Key3_Press_Done
	mov active_profile, #1
	ret
Handle_Key3_Press_Done:
	inc active_profile
	ret
Handle_Key2_Press:
	cpl display_mode
	ret
Handle_Key1_Press:
	cpl start_time
	ret

; Checks if single key is pressed using analog signal.
; %0 -> Voltage constant.
; %1 -> Pressed state true function addr.
; %2 -> Pressed state false jump addr.
Is_Key_Pressed MAC
	clr c
	mov a, AD0DAT0
	subb a, #(%0 - 10) ; Threshold Voltage=%0*(3.3/255); the -10 is to prevent false readings
	jc %2
	; Debounce
	Wait_Milli_Seconds(#50) ; Debounce delay.  This macro is also in 'LCD_4bit.inc'
	clr c
	mov a, AD0DAT0
	subb a, #(%0 - 10) ; Threshold Voltage=%0*(3.3/255); the -10 is to prevent false readings
	jc %2
Debounce_Check_%M:
	clr c
	mov a, AD0DAT0
	subb a, #(%0 - 10) ; Threshold Voltage=%0*(3.3/255); the -10 is to prevent false readings
	jnc Debounce_Check_%M
	lcall %1
	ret
ENDMAC

; Uses analog voltage reading from AD0DAT0 to determine
; if key is pressed.
Detect_Keypress:
	Is_Key_Pressed(216, Handle_Key7_Press, ADC_to_PB_L6)
ADC_to_PB_L6:
	Is_Key_Pressed(185, Handle_Key6_Press, ADC_to_PB_L5)
ADC_to_PB_L5:
	Is_Key_Pressed(154, Handle_Key5_Press, ADC_to_PB_L4)
ADC_to_PB_L4:
	Is_Key_Pressed(123, Handle_Key4_Press, ADC_to_PB_L3)
ADC_to_PB_L3:
	Is_Key_Pressed(92, Handle_Key3_Press, ADC_to_PB_L2)
ADC_to_PB_L2:
	Is_Key_Pressed(61, Handle_Key2_Press, ADC_to_PB_L1)
ADC_to_PB_L1:
	Is_Key_Pressed(30, Handle_Key1_Press, ADC_to_PB_L0)
ADC_to_PB_L0:
	; No pusbutton pressed	
	ret

Status_Line1: db 'Welcome   State ', 0
Status_Line2: db '00:00 xxxC      ', 0
Profile_Line1: db 'Rt:      C Rs:  ', 0
Profile_Line2: db 'St:      C Ss:   ', 0

Draw_0:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'0'
	lcall ?WriteData
	ret
	
Draw_1:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'1'
	lcall ?WriteData
	ret
	
Draw_2:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'2'
	lcall ?WriteData
	ret
	
Draw_3:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'3'
	lcall ?WriteData
	ret
	
Draw_4:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'4'
	lcall ?WriteData
	ret
	
Draw_5:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'5'
	lcall ?WriteData
	ret
	
Draw_6:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'6'
	lcall ?WriteData
	ret
	
Draw_7:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'7'
	lcall ?WriteData
	ret
	
Draw_8:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'8'
	lcall ?WriteData
	ret
	
Draw_9:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'9'
	lcall ?WriteData
	ret

; The number to display is passed in accumulator.  The column where to display the
; number is passed in R1. This works only for numbers 0 to 9.
Display_number:
	; We need to multiply the accumulator by 3 because the jump table below uses 3 bytes
	; for each 'ljmp' instruction.
	mov b, #3
	mul ab
	mov dptr, #Jump_number
	jmp @A+dptr
Jump_number:
	ljmp Draw_0 ; This instruction uses 3 bytes
	ljmp Draw_1
	ljmp Draw_2
	ljmp Draw_3
	ljmp Draw_4
	ljmp Draw_5
	ljmp Draw_6
	ljmp Draw_7
	ljmp Draw_8
	ljmp Draw_9

; Takes a BCD 2-digit number passed in the accumulator and displays it at position passed in R0
Display_mini_BCD:
	push acc
	; Display the most significant decimal digit
	mov b, R0
	mov R1, b
	swap a
	anl a, #0x0f
	lcall Display_number
	
	; Display the least significant decimal digit, which starts 4 columns to the right of the most significant digit
	mov a, R0
	add a, #1
	mov R1, a
	pop acc
	anl a, #0x0f
	lcall Display_number
	
	ret

Render:
	push acc
	push ar0
	jb display_mode, Render_Profile
	Set_Cursor(1, 1)
	Send_Constant_String(#Status_Line1)
	Set_Cursor(2, 1)
	Send_Constant_String(#Status_Line2)

	lcall Display_ADC_Values
	Set_Cursor(2, 1)
	mov a, minute
	lcall LCD_number
	clr a
	mov a, seconds
	Set_Cursor(2, 4)
	lcall LCD_number
	mov a, curr_state
	Set_Cursor(2,14)
	lcall LCD_number
	mov a,r5
	Set_Cursor(2,12)
	lcall LCD_number
	ljmp Render_Done
Render_Profile:
	Set_Cursor(1, 1)
	Send_Constant_String(#Profile_Line1)
	Set_Cursor(2, 1)
	Send_Constant_String(#Profile_Line2)


	mov r0, #0x84
	lcall Get_Reflow_Temp
	lcall hex2bcd
	mov a,bcd+1
	lcall Display_mini_BCD
	mov r0, #0x86
	mov a,bcd
	lcall Display_mini_BCD
	
	mov r0, #0x8e
	lcall Get_Reflow_Time
	da a
	lcall Display_mini_BCD
	
	mov r0, #0xc4
	lcall Get_Soak_Temp
	lcall hex2bcd
	mov a,bcd+1
	lcall Display_mini_BCD
	mov r0, #0xc6
	mov a,bcd
	lcall Display_mini_BCD
	
	mov r0, #0xce
	lcall Get_Soak_time
	da a
	lcall Display_mini_BCD
Render_Done:
	pop ar0
	pop acc
	ret

Reset:
	clr TR1 ; Stop timer 1
	
	clr TMOD20 ; Stop CCU timer
	setb EA ; Enable global interrupts.
	
	clr SOUND ; Turn speaker off

	mov soaktemp_p1+0, #low(120)
	mov soaktemp_p1+1, #high(120)
	mov soaktime_p1, #0x65
	mov reflowtemp_p1+0, #low(240)
	mov reflowtemp_p1+1, #high(240)
	mov reflowtime_p1, #0x35

;	mov soaktemp_p2+0, #low(135)
;	mov soaktemp_p2+1, #high(135)
;	mov soaktime_p2, #0x70
;	mov reflowtemp_p2+0, #low(250)
;	mov reflowtemp_p2+1, #high(250)
;	mov reflowtime_p2, #0x45

	;mov soaktemp_p3+0, #low(115)
	;mov soaktemp_p3+1, #high(115)
	;mov soaktime_p3, #0x80
	;mov reflowtemp_p3+0, #low(255)
	;mov reflowtemp_p3+1, #high(255)
	;mov reflowtime_p3, #0x30
	mov active_profile, #1

	; Initialize variables
	; clr T2S_FSM_Start
	; mov T2S_FSM_state, #0
	mov minute, #0
	mov seconds, #0
	mov r5,#0
	mov curr_state, #0
	clr start_time
	clr display_mode

	setb TR1 ; Start timer 1
	ret
	
;---------------------------------;
; Main program. Includes hardware ;
; initialization and 'forever'    ;
; loop.                           ;
;---------------------------------;
MainProgram:
	mov SP, #0x7F

	lcall Ports_Init ; Default all pins as bidirectional I/O. See Table 42.
	lcall LCD_4BIT
	lcall Double_Clk
	lcall InitSerialPort
	lcall InitADC0 ; Call after 'Ports_Init'
	lcall InitDAC1 ; Call after 'Ports_Init'
	lcall CCU_Init
	lcall Init_SPI
	lcall Timer1_Init
	setb P3.1
	lcall Reset
	mov curr_state, #0
	mov r5,#0
    
; Test that we can play any sound from the index
;	mov a, #26
;	lcall Play_Sound_Using_Index
;	jb TMOD20, $ ; Wait for sound to finish playing

; Test that we can play any minutes:seconds combination properly (although for 01:01 it says 'one minutes one seconds')
;	mov minute, #25
;	mov seconds, #37
;	setb T2S_FSM_Start
	
forever_loop:


	ljmp forever_loop

END
