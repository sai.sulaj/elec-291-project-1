; Idea:
; Read our current temperature
; With some function that corresponds to our current state and profile
; retreive the desired temperature
; and infinitely loop via comparison


power_pin equ P3.1

MainSSRProgram:	
; via function we're going to store into return_for_ssr the desired value 
MainSSRLoop:
	; function call here to get desired temp/value into accumalator
	Load_X(Curr_temp)
	Load_y(return_for_ssr)
	lcall x_lt_y ; variable mf = 1 if x < y
	jb mf, less_than
	setb power_pin
	ret

less_than:
	clr power_pin
	ret
   

