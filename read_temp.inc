
Wait1S:
	Wait_Milli_Seconds(#250)
	Wait_Milli_Seconds(#250)
	ret

InitSerialPort:
	mov	BRGCON,#0x00
	mov	BRGR1,#high(BRVAL)
	mov	BRGR0,#low(BRVAL)
	mov	BRGCON,#0x03 ; Turn-on the baud rate generator
	mov	SCON,#0x52 ; Serial port in mode 1, ren, txrdy, rxempty
	mov	P1M1,#0x00 ; Enable pins RxD and TXD
	mov	P1M2,#0x00 ; Enable pins RxD and TXD
	ret



InitialMessage: db '\rADC Tester: Prints every 0.5 seconds\r\n', 0
adc_msg: db '\rADC: ', 0
adc_msg_lcd: db 'ADC: ', 0

read_temp:
    mov SP, #0x7F
	lcall Ports_Init
    lcall InitSerialPort
	lcall InitADC1
	lcall LCD_4BIT
	lcall Wait1S ; Wait a bit so PUTTy has a chance to start
	mov dptr, #InitialMessage
	lcall SendString
    Set_Cursor(1,1)
    Send_Constant_String(#adc_msg_lcd)	

forever_loop:
    ; Send the conversion results via the serial port to putty.
	mov a, #'\r' ; move cursor all the way to the left
    lcall putchar
    ; Display converted value from P0.1
	mov	b, AD1DAT0
	mov cunt, b

    mov     dptr, #adc_msg
    lcall   SendString
    mov     x+0, cunt+0
    mov     x+1, cunt+1
    mov     x+2, #0
    mov     x+3, #0
    lcall   hex2bcd
    
    mov     dptr, #adc_msg
    lcall   SendString
    Send_BCD(bcd+3)
    Send_BCD(bcd+2)
    Send_BCD(bcd+1)
    Send_BCD(bcd+0)
    mov a, #'\r' ; move cursor all the way to the left
    lcall putchar
    mov a, #'\n' ; move cursor all the way to the left
    lcall putchar

    Set_Cursor(2,1)
    Display_BCD(bcd+3)
    Display_BCD(bcd+2)
    Display_BCD(bcd+1)
    Display_BCD(bcd+0)

    lcall 	Wait1s
	ret

end
