; gonna use variable: Curr_state 
; gonna use keep track of profile via variable: profile

; main idea is to
; first: iterate to check which profile we are in, 
; second: iterate which Curr_state we are in per profile
; after we know which profile and Curr_state we are in, assign corresponding value to return_for_ssr
; if somehow none of the states and profiles match then we will still return 0 to return_for_ssr

; note:
; we want to hit temp, then stay at temp, then hit next temp, and stay at that temp, then go back to 0, stay at 0 


;need


for_ssr_function:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; checks which profile we in
for_ssr_function_check:
    ; want to 1st check which profile we are in 
    clr c
    mov a, #0 
    mov R7, profile
    subb a, R7
    cjne a, #00, which_profile_2 ; if its not 0, we not in that profile
    ljmp we_in_profile_0

which_profile_2:
    mov a, #1 
    mov R7, profile
    subb a, R7
    cjne a, #00, which_profile_3
    ljmp we_in_profile_1


which_profile_3: 
    clr c
    mov a, #2 
    mov R7, profile
    subb a, R7
    cjne a, #00, which_profile_4
    ljmp we_in_profile_2


which_profile_4:    
    ; if we are somehow not in any of the states, assign return_for_ssr value 0
    mov a, #00
    mov return_for_ssr+0, a
    mov a, #00
    mov return_for_ssr+1, a
    ret 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; checks which Curr_state we in for profile 1 
; once we know what profile we are in we wanna check what Curr_state we are in 
we_in_profile_0:
    clr c
    mov a, #0 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_0_1 ; if a is not equal to 0, means we not in this Curr_state, so go to next Curr_state check
    ljmp p1_state_0

which_state_profile_0_1:
    clr c
    mov a, #1 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_0_2
    ljmp p1_state_1

which_state_profile_0_2:
    clr c
    mov a, #2 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_0_3
    ljmp p1_state_2

which_state_profile_0_3:
    clr c
    mov a, #3 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_0_4
    ljmp p1_state_3

which_state_profile_0_4:
    clr c
    mov a, #4 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_0_5
    ljmp p1_state_4

which_state_profile_0_5:
    clr c
    mov a, #5 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_0_6
    ljmp p1_state_5

which_state_profile_0_6:
    ; if we are somehow not in any of the states, assign return_for_ssr value 0
    mov a, #00
    mov return_for_ssr+0, a
    mov a, #00
    mov return_for_ssr+1, a
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; checks which Curr_state we in for profile 2

we_in_profile_1:
    clr c
    mov a, #0 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_1_1 ; if a is not equal to 0, means we not in this Curr_state, so go to next Curr_state check
    ljmp p2_state_0

which_state_profile_1_1:
    clr c
    mov a, #1 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_1_2
    ljmp p2_state_1

which_state_profile_1_2:
    clr c
    mov a, #2 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_1_3
    ljmp p2_state_2

which_state_profile_1_3:
    clr c
    mov a, #3 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_1_4
    ljmp p2_state_3

which_state_profile_1_4:
    clr c
    mov a, #4 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_1_5
    ljmp p2_state_4

which_state_profile_1_5:
    clr c
    mov a, #5 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_1_6
    ljmp p2_state_5

which_state_profile_1_6:
    ; if we are somehow not in any of the states, assign return_for_ssr value 0
    mov a, #00
    mov return_for_ssr+0, a
    mov a, #00
    mov return_for_ssr+1, a
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; checks which Curr_state we in for profile 3


we_in_profile_2:
    clr c
    mov a, #0 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_2_1 ; if a is not equal to 0, means we not in this Curr_state, so go to next Curr_state check
    ljmp p3_state_0

which_state_profile_2_1:
    clr c
    mov a, #1 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_2_2
    ljmp p3_state_1

which_state_profile_2_2:
    clr c
    mov a, #2 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_2_3
    ljmp p3_state_2

which_state_profile_2_3:
    clr c
    mov a, #3 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_2_4
    ljmp p3_state_3

which_state_profile_2_4:
    clr c
    mov a, #4 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_2_5
    ljmp p3_state_4

which_state_profile_2_5:
    clr c
    mov a, #5 
    mov R7, Curr_state 
    subb a, R7
    cjne a, #00, which_state_profile_2_6
    ljmp p3_state_5

which_state_profile_2_6:
    ; if we are somehow not in any of the states, assign return_for_ssr value 0
    mov a, #00
    mov return_for_ssr+0, a
    mov a, #00
    mov return_for_ssr+1, a
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; checks which Curr_state we in for profile 4


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; assigns values accordingly
; assign values into variable two bit variable called return_for_ssr


; here we need to assign corresponding values into return_for_ssr
; if we in p1_state_1, we wanna return the value we want to reach for p1_state_2 
; if u know what i mean lOL , like if we at 0 temp in Curr_state 1, we want to return the temp for the next Curr_state which
; we want to continually compare to until we hit it haha 


p1_state_0:
    mov a, soaktemp_p1+0
    mov return_for_ssr+0, a
    mov a, soaktemp_p1+1
    mov return_for_ssr+1, a 
    ret
p1_state_1:
    mov a, soaktemp_p1+0
    mov return_for_ssr+0, a
    mov a, soaktemp_p1+1
    mov return_for_ssr+1, a 

    ret
p1_state_2:
    mov a, reflowtemp_p1+0
    mov return_for_ssr+0, a
    mov a, reflowtemp_p1+1
    mov return_for_ssr+1, a 
    ret

p1_state_3:
    mov a, reflowtemp_p1+0
    mov return_for_ssr+0, a
    mov a, reflowtemp_p1+1
    mov return_for_ssr+1, a 

    ret
p1_state_4:
    mov a, #00
    mov return_for_ssr+0, a
    mov a, #00
    mov return_for_ssr+1, a 

    ret
p1_state_5:
    mov a, #00
    mov return_for_ssr+0, a
    mov a, #00
    mov return_for_ssr+1, a 

    ret



p2_state_0:
    mov a, soaktemp_p2+0
    mov return_for_ssr+0, a
    mov a, soaktemp_p2+1
    mov return_for_ssr+1, a 

    ret
p2_state_1:
    mov a, soaktemp_p2+0
    mov return_for_ssr+0, a
    mov a, soaktemp_p2+1
    mov return_for_ssr+1, a 

    ret
p2_state_2:
    mov a, reflowtemp_p2+0
    mov return_for_ssr+0, a
    mov a, reflowtemp_p2+1
    mov return_for_ssr+1, a

    ret
p2_state_3:
    mov a, reflowtemp_p2+0
    mov return_for_ssr+0, a
    mov a, reflowtemp_p2+1
    mov return_for_ssr+1, a

    ret
p2_state_4:
    mov a, #00
    mov return_for_ssr+0, a
    mov a, #00
    mov return_for_ssr+1, a 

    ret
p2_state_5:
    mov a, #00
    mov return_for_ssr+0, a
    mov a, #00
    mov return_for_ssr+1, a 

    ret



p3_state_0:
    mov a, soaktemp_p3+0
    mov return_for_ssr+0, a
    mov a, soaktemp_p3+1
    mov return_for_ssr+1, a

    ret
p3_state_1:
    mov a, soaktemp_p3+0
    mov return_for_ssr+0, a
    mov a, soaktemp_p3+1
    mov return_for_ssr+1, a

    ret
p3_state_2:
    mov a, reflowtemp_p3+0
    mov return_for_ssr+0, a
    mov a, reflowtemp_p3+1
    mov return_for_ssr+1, a

    ret
p3_state_3:
    mov a, reflowtemp_p3+0
    mov return_for_ssr+0, a
    mov a, reflowtemp_p3+1
    mov return_for_ssr+1, a

    ret
p3_state_4:
    mov a, #00
    mov return_for_ssr+0, a
    mov a, #00
    mov return_for_ssr+1, a

    ret
p3_state_5:
    mov a, #00
    mov return_for_ssr+0, a
    mov a, #00
    mov return_for_ssr+1, a

    ret








