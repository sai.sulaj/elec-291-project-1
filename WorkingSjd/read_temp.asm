$MOD9351

CLK           EQU 14746000  ; Microcontroller system crystal frequency in Hz
TIMER0_RATE   EQU 2000     ; 1000Hz, for a timer tick of 1ms
TIMER0_RELOAD EQU ((65536-(CLK/(2*TIMER0_RATE))))
XTAL EQU 7373000
BAUD EQU 115200
BRVAL EQU ((XTAL/BAUD)-16)

org 0x0000
	ljmp	MainProgram

; Timer/Counter 0 overflow interrupt vector
org 0x000B
	ljmp Timer0_ISR

dseg at 0x30
; Current FSM state.
Curr_State: ds 1
; Time is reset to 0 on some state transitions due to storage limitations.
; 16 bits, max: 65 seconds.
Count1ms: ds 2
BCDcounter: ds 1
x:   ds 4
y:   ds 4
bcd: ds 5
profile: ds 1
cunt: ds 4
soaktemp_p1: ds 2
soaktime_p1: ds 1
reflowtemp_p1: ds 2								; Needs to be able to hold numbers larger than 255 -> 16 bit instead of 8 bit
reflowtime_p1: ds 1

soaktemp_p2: ds 2
soaktime_p2: ds 1
reflowtemp_p2: ds 2								; Needs to be able to hold numbers larger than 255 -> 16 bit instead of 8 bit
reflowtime_p2: ds 1

soaktemp_p3: ds 2
soaktime_p3: ds 1
reflowtemp_p3: ds 2								; Needs to be able to hold numbers larger than 255 -> 16 bit instead of 8 bit
reflowtime_p3: ds 1

return_for_ssr: ds 2							; holds return value for ssr function
Currpro_soaktime: ds 1
Currpro_reflowtime: ds 1
Curr_temp: ds 2


bseg
mf: dbit 1
seconds_flag: dbit 1
five_seconds_flag: dbit 1
screen_sel: dbit 1
permission: dbit 1


cseg
; These 'equ' must match the hardware wiring
LCD_RS equ P0.5
LCD_RW equ P0.6
LCD_E  equ P0.7
LCD_D4 equ P1.2
LCD_D5 equ P1.3
LCD_D6 equ P1.4
LCD_D7 equ P1.6
DEBUG equ P2.1
power_pin equ P3.1

$NOLIST
$include(LCD_4bit_LPC9351.inc) 					; A library of LCD related functions and utility macros
$LIST

$NOLIST					
$include(math32.inc)							; A library of Math functions
$LIST

$NOLIST
$include(Display.inc)
$LIST

$NOLIST
$include(SSR_function_call.inc)
$LIST

$NOLIST
$include(temp_func_call.inc)
$LIST

;---------------------------------;
; Routine to initialize the ISR   ;
; for timer 0                     ;
;---------------------------------;
Timer0_Init:
	mov a, TMOD
	anl a, #0xf0 ; Clear the bits for timer 0
	orl a, #0x01 ; Configure timer 0 as 16-timer
	mov TMOD, a
	mov TH0, #high(TIMER0_RELOAD)
	mov TL0, #low(TIMER0_RELOAD)
	; Enable the timer and interrupts
	setb ET0  ; Enable timer 0 interrupt
	setb TR0  ; Start timer 0
	ret

;---------------------------------;
; ISR for timer 0.  Runs every ms ;
;---------------------------------;
Timer0_ISR:
	mov TH0, #high(TIMER0_RELOAD)
	mov TL0, #low(TIMER0_RELOAD)
	; The two registers used in the ISR must be saved in the stack
	push acc
	push psw
	; jnb permission, Inc_Done
	inc Count1ms+0    							; Increment the low 8-bits first
	mov a, Count1ms+0 							; If the low 8-bits overflow, then increment high 8-bits
	jnz Inc_Done
	inc Count1ms+1
Inc_Done:
	;check if second has passed
	mov a,Count1ms+0
	cjne a,#low(1000),TIMER0_ISR_done
	mov a,Count1ms+1
	cjne a,#high(1000),TIMER0_ISR_done
	; Its been a second
	setb seconds_flag
	clr a										; Clear Count1ms
	mov Count1ms+0, a
	mov Count1ms+1, a
	mov a, BCDcounter							; Increment BCDcounter
	add a , #0x01
	da a
	mov BCDcounter, a							; Store updated BCDcounter
TIMER0_ISR_done:
	pop psw
	pop acc
	reti

InitSerialPort:
	mov	BRGCON,#0x00
	mov	BRGR1,#high(BRVAL)
	mov	BRGR0,#low(BRVAL)
	mov	BRGCON,#0x03 ; Turn-on the baud rate generator
	mov	SCON,#0x52 ; Serial port in mode 1, ren, txrdy, rxempty
	orl	P0M1,#00000011b
	anl	P0M2,#11111100b
	ret

Ports_Init:
    ; Configure all the ports in bidirectional mode:
    mov P0M1, #00H
    mov P0M2, #00H
    mov P1M1, #00H
    mov P1M2, #00H 								; WARNING: P1.2 and P1.3 need 1 kohm pull-up resistors if used as outputs!
    mov P2M1, #00H
    mov P2M2, #00H
    mov P3M1, #00H
    mov P3M2, #00H
	ret

InitADC0:
	; ADC0_0 is connected to P1.7
	; ADC0_1 is connected to P0.0
	; ADC0_2 is connected to P2.1
	; ADC0_3 is connected to P2.0
	; Configure pins P1.7, P0.0, P2.1, and P2.0 as inputs
	orl P0M1, #00000001b
	anl P0M2, #11111110b
	orl P1M1, #10000000b
	anl P1M2, #01111111b
	; orl P2M1, #00000011b
	; anl P2M2, #11111100b
	; Setup ADC0
	setb BURST0 ; Autoscan continuos conversion mode
	mov	ADMODB,#0x20 ;ADC0 clock is 7.3728MHz/2
	mov	ADINS,#00000001b ; Select the four channels of ADC0 for conversion
	mov	ADCON0,#0x05 ; Enable the converter and start immediately
	; Wait for first conversion to complete
InitADC0_L1:
	mov	a,ADCON0
	jnb	acc.3,InitADC0_L1
	ret

InitADC1:
    ; Configure pins P0.4, P0.3, P0.2, and P0.1 as inputs
	orl	P0M1,#00000010b
	anl	P0M2,#11111101b
	setb 	BURST1 ; Autoscan continuos conversion mode
	mov	ADMODB,#0x20 ;ADC1 clock is 7.3728MHz/2
	mov	ADINS,#0x13; Select the four channels for conversion
	mov	ADCON1,#0x05 ; Enable the converter and start immediately
	; Wait for first conversion to complete
InitADC1_L1:
	mov	a,ADCON1
	jnb	acc.3,InitADC1_L1
	ret
	
; Functions to handle key presses.
; MAKE SURE YOU RETURN, AND PUSH/POP ANY MUTATED REGISTERS.
Handle_Key7_Press:
	cpl permission
	cpl TR0
	ret
Handle_Key6_Press:
	ret
Handle_Key5_Press:
	ret
Handle_Key4_Press:
	ret
Handle_Key3_Press:
;; change profile
	mov a, profile
	add a, #0x01
	cjne a, #3, next_profile
	clr a
next_profile:
	mov profile, a	
	ret
Handle_Key2_Press:
	cpl screen_sel
	lcall clear_screen
	ret
Handle_Key1_Press:
	;; Reset has been pressed, so reset everything
	clr TR0 									; Stop CCU timer
	clr a
	mov Count1ms + 0, a
	mov Count1ms + 1, a
	mov BCDcounter, a
	mov Curr_State, a
	clr screen_sel
	clr permission	
	; setb TR0						; We dont want to start timer after reset
	lcall clear_screen
	lcall loop_b
	ret

; Checks if single key is pressed using analog signal.
; %0 -> Voltage constant.
; %1 -> Pressed state true function addr.
; %2 -> Pressed state false jump addr.
Is_Key_Pressed MAC
	clr c
	mov a, AD0DAT1
	subb a, #(%0 - 10) ; Threshold Voltage=%0*(3.3/255); the -10 is to prevent false readings
	jc %2
	; Debounce
	Wait_Milli_Seconds(#50) ; Debounce delay.  This macro is also in 'LCD_4bit.inc'
	clr c
	mov a, AD0DAT1
	subb a, #(%0 - 10) ; Threshold Voltage=%0*(3.3/255); the -10 is to prevent false readings
	jc %2
Debounce_Check_%M:
	clr c
	mov a, AD0DAT1
	subb a, #(%0 - 10) ; Threshold Voltage=%0*(3.3/255); the -10 is to prevent false readings
	jnc Debounce_Check_%M
	lcall %1
	ret
ENDMAC

; Uses analog voltage reading from AD0DAT1 to determine
; if key is pressed.
Detect_Keypress:
	Is_Key_Pressed(216, Handle_Key7_Press, ADC_to_PB_L6)
ADC_to_PB_L6:
	Is_Key_Pressed(185, Handle_Key6_Press, ADC_to_PB_L5)
ADC_to_PB_L5:
	Is_Key_Pressed(154, Handle_Key5_Press, ADC_to_PB_L4)
ADC_to_PB_L4:
	Is_Key_Pressed(123, Handle_Key4_Press, ADC_to_PB_L3)
ADC_to_PB_L3:
	Is_Key_Pressed(92, Handle_Key3_Press, ADC_to_PB_L2)
ADC_to_PB_L2:
	Is_Key_Pressed(61, Handle_Key2_Press, ADC_to_PB_L1)
ADC_to_PB_L1:
	Is_Key_Pressed(30, Handle_Key1_Press, ADC_to_PB_L0)
ADC_to_PB_L0:
	; No pusbutton pressed	
	ret

HexAscii: db '0123456789ABCDEF'
;---------------------------------;
; Send a BCD number to PuTTY      ;
;-----------------------------

InitialMessage: db '\rADC Tester: Prints every 0.5 seconds\r\n', 0
adc_msg: db '\rADC: ', 0
adc_msg_lcd: db 'ADC:UWU ', 0

read_Temp:
	; Send the conversion results via the serial port to putty.
	; mov a, #'\r' ; move cursor all the way to the left
	; lcall putchar
	; Display converted value from P0.0
	mov b, AD0DAT0
	mov cunt, b

	cpl DEBUG
	Wait_Milli_Seconds(#250)
	Wait_Milli_Seconds(#250)

	mov x+0, cunt+0
	mov x+1, cunt+1
	mov x+2, #0
	mov x+3, #0
	Load_y(3267)
	lcall mul32
	Load_y(255)
	lcall div32
	; Load_y(50000)
	; lcall mul32
	; Load_y(533)
	; lcall div32
	; Load_y(567)
	; lcall sub32
	Send_BCD(bcd)
	mov Curr_temp, x
	mov Curr_temp+1, x+1

	ret

MainProgram:
   	; Initialization of hardware
	mov SP, #0x7F
	
	clr screen_sel
	lcall Ports_Init
	lcall InitSerialPort
	lcall LCD_4BIT
	lcall Timer0_Init
	lcall InitADC0
	; lcall InitADC1
	lcall Custom_Characters
	; clr TMOD20 ; Stop CCU timer
	setb EA
	setb seconds_flag
	clr five_seconds_flag
	clr permission
	
	; clr TR0
	mov soaktemp_p1+0, #low(120)
	mov soaktemp_p1+1, #high(120)
	mov soaktime_p1, #0x65
	mov reflowtemp_p1+0, #low(240)
	mov reflowtemp_p1+1, #high(240)
	mov reflowtime_p1, #0x35

	mov soaktemp_p2+0, #low(135)
	mov soaktemp_p2+1, #high(135)
	mov soaktime_p2, #0x70
	mov reflowtemp_p2+0, #low(250)
	mov reflowtemp_p2+1, #high(250)
	mov reflowtime_p2, #0x45

	mov soaktemp_p3+0, #low(115)
	mov soaktemp_p3+1, #high(115)
	mov soaktime_p3, #0x80
	mov reflowtemp_p3+0, #low(255)
	mov reflowtemp_p3+1, #high(255)
	mov reflowtime_p3, #0x30
	mov profile, #0x00
	mov BCDcounter, #0x00
     
	; Initialize variables
	mov Curr_State, #0
	mov Curr_temp, #low(101)
	mov Curr_temp+1,#high(101)
	mov Count1ms, #0
forever_loop:
	lcall read_Temp	
State_permission:
	; lcall Detect_Keypress
	; lcall MainSSRLoop
	lcall Display_screen
	; jb permission,permission_granted
;; no permission
	; ljmp State_permission
permission_granted:

	; mov a, Curr_State
	; clr power_pin
State0:
	; cjne a, #0, State1
	; read current temp
	; lcall for_ssr_function
	; jb power_pin,State1
	; mov a, Curr_State
	; add a, #0x1
	; mov Curr_State,a 
	; clr a
	; mov BCDcounter, a
State1:
	; cjne a, #1, State2	
	; lcall for_ssr_function
	; clr c
	; Load_y(Currpro_soaktime)
	; Load_x(BCDcounter)
	; lcall x_lt_y			;; if we havent hit reflow time yet -> mf = 1
	; jb mf, State2
	; mov a, Curr_State
	; add a, #0x1
	; mov Curr_State,a 
	; clr a
	; mov BCDcounter, a
State2:
	; cjne a, #2, State3
	; ; read current temp
	; lcall for_ssr_function
	; jb power_pin,State3
	; mov a, Curr_State
	; add a, #0x1
	; mov Curr_State,a 
	; clr a
	; mov BCDcounter, a
State3:
	; cjne a, #3, State4
	; lcall for_ssr_function
	; clr c
	; Load_y(Currpro_reflowtime)
	; Load_x(BCDcounter)
	; lcall x_lt_y			;; if we havent hit reflow time yet -> mf = 1
	; jb mf, State4
	; mov a, Curr_State
	; add a, #0x1
	; mov Curr_State,a 
	; clr a
	; mov BCDcounter, a
State4:
	; cjne a, #4, State5
	; clr c
	; Load_y(0x30)
	; Load_x(Curr_temp)
	; lcall x_gt_y			
	; jb mf, State5
	; mov a, Curr_State
	; add a, #0x1
	; mov Curr_State,a 
	; clr a
	; mov BCDcounter, a	
State5:
	; cjne a, #5, StateEnd
	; clr permission
	; clr a
	; mov Curr_State, a
	; mov BCDcounter, a
StateEnd:
	ljmp forever_loop

end
