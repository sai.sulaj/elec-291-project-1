;; All Display functionality code for cleaner Main

display_status:
	lcall send_status
	jnb seconds_flag,not_a_second
loop_b:
	clr seconds_flag
	mov r0, #0x8e
	mov a,BCDcounter
	lcall Display_mini_BCD

	mov x, Curr_temp
    mov x+1, Curr_temp+1
    mov x+2, #0x00
    mov x+3, #0x00
	lcall hex2bcd
	mov r0, #0xc6
	mov a,bcd+1
	lcall Display_mini_BCD
	mov r0, #0xc8
	mov a,bcd
	lcall Display_mini_BCD
	
	mov r0, #0x86
	mov a, Curr_State 
	lcall Display_mini_BCD
not_a_second:
	ret
    
display_profile:
	push acc
	push ar0
	lcall send_profile
	; We need to multiply the accumulator by 3 because the jump table below uses 3 bytes
	; for each 'ljmp' instruction.
	mov a, profile
	mov b, #3
	mul ab
	mov dptr, #Jump_profile
	jmp @A+dptr
Jump_profile:
	ljmp display_p1_data
	ljmp display_p2_data
	ljmp display_p3_data
profile_return:
	pop ar0
	pop acc
	ret

Custom_Characters:
	WriteCommand(#40h) ; Custom characters are stored starting at address 40h
; Custom made character 0
	WriteData(#00110B)
	WriteData(#01001B)
	WriteData(#01001B)
	WriteData(#00110B)
	WriteData(#00000B)
	WriteData(#00000B)
	WriteData(#00000B)
	WriteData(#00000B)
	ret

Draw_degree:
	mov a,r0
	lcall ?WriteCommand
	WriteData(#0)
	ret
	
Draw_0:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'0'
	lcall ?WriteData
	ret
	
Draw_1:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'1'
	lcall ?WriteData
	ret
	
Draw_2:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'2'
	lcall ?WriteData
	ret
	
Draw_3:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'3'
	lcall ?WriteData
	ret
	
Draw_4:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'4'
	lcall ?WriteData
	ret
	
Draw_5:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'5'
	lcall ?WriteData
	ret
	
Draw_6:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'6'
	lcall ?WriteData
	ret
	
Draw_7:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'7'
	lcall ?WriteData
	ret
	
Draw_8:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'8'
	lcall ?WriteData
	ret
	
Draw_9:
	mov a,R1
	lcall ?WriteCommand
	mov a,#'9'
	lcall ?WriteData
	ret

; The number to display is passed in accumulator.  The column where to display the
; number is passed in R1. This works only for numbers 0 to 9.
Display_number:
	; We need to multiply the accumulator by 3 because the jump table below uses 3 bytes
	; for each 'ljmp' instruction.
	mov b, #3
	mul ab
	mov dptr, #Jump_number
	jmp @A+dptr
Jump_number:
	ljmp Draw_0 ; This instruction uses 3 bytes
	ljmp Draw_1
	ljmp Draw_2
	ljmp Draw_3
	ljmp Draw_4
	ljmp Draw_5
	ljmp Draw_6
	ljmp Draw_7
	ljmp Draw_8
	ljmp Draw_9
	
; Takes a BCD 2-digit number passed in the accumulator and displays it at position passed in R0
Display_mini_BCD:
	push acc
	; Display the most significant decimal digit
	mov b, R0
	mov R1, b
	swap a
	anl a, #0x0f
	lcall Display_number
	
	; Display the least significant decimal digit, which starts 4 columns to the right of the most significant digit
	mov a, R0
	add a, #1
	mov R1, a
	pop acc
	anl a, #0x0f
	lcall Display_number
	
	ret
	
state_message:
	db 'State: ', 0
temp_message:
	db 'CTemp: ', 0
time_message:
	db 'Time:', 0
stemp_message:
	db 'S :', 0
stime_message:
	db 'St:', 0
rtemp_message:
	db 'R :', 0
rtime_message:
	db 'Rt:', 0
clear:
	db '                ', 0
	
Display_screen:
	push acc
	push ar0
	jb screen_sel, profile_mode1
	lcall display_status
	sjmp display_return
profile_mode1:
	lcall display_profile	
display_return:
	pop ar0
	pop acc
	ret
	
clear_screen:
	WriteCommand(#0x80)
	Send_Constant_String(#clear)
	WriteCommand(#0xc0)
	Send_Constant_String(#clear)
	ret

send_status:
	push acc
	push ar0
	WriteCommand(#0x80)
	Send_Constant_String(#state_message)
	
	WriteCommand(#0xc0)
	Send_Constant_String(#temp_message)
	mov r0, #0xca
	lcall Draw_degree
	
	WriteCommand(#0x89)
	Send_Constant_String(#time_message)
	pop ar0
	pop acc
	ret
	
send_profile:
	WriteCommand(#0x80)
	Send_Constant_String(#stemp_message)
	mov r0, #0x81
	lcall Draw_degree
	WriteCommand(#0x8a)
	Send_Constant_String(#stime_message)
	WriteCommand(#0xc0)
	Send_Constant_String(#rtemp_message)
	mov r0, #0xc1
	lcall Draw_degree
	WriteCommand(#0xca)
	Send_Constant_String(#rtime_message)
	ret 
	
display_p1_data:	
	mov r0, #0x84
	mov x,soaktemp_p1
	mov x+1,soaktemp_p1+1
	mov x+2,#0
	mov x+3,#0
	lcall hex2bcd

	mov a,bcd+1
	lcall Display_mini_BCD
	mov r0, #0x86
	mov a,bcd
	lcall Display_mini_BCD
	
	mov r0, #0x8e
	mov a, soaktime_p1
	lcall Display_mini_BCD
	mov Currpro_soaktime, a
	
	mov r0, #0xc4
	mov x,reflowtemp_p1
	mov x+1,reflowtemp_p1+1
	mov x+2,#0
	mov x+3,#0
	lcall hex2bcd
	mov a,bcd+1
	lcall Display_mini_BCD
	mov r0, #0xc6
	mov a,bcd
	lcall Display_mini_BCD
	
	mov r0, #0xce
	mov a, reflowtime_p1
	lcall Display_mini_BCD
	mov Currpro_reflowtime, a
	ljmp profile_return
	
display_p2_data:	
	mov r0, #0x84
	mov x,soaktemp_p2
	mov x+1,soaktemp_p2+1
	mov x+2,#0
	mov x+3,#0
	lcall hex2bcd
	mov a,bcd+1
	lcall Display_mini_BCD
	mov r0, #0x86
	mov a,bcd
	lcall Display_mini_BCD
	
	mov r0, #0x8e
	mov a, soaktime_p2
	lcall Display_mini_BCD
	mov Currpro_soaktime, a
	
	mov r0, #0xc4
	mov x,reflowtemp_p2
	mov x+1,reflowtemp_p2+1
	mov x+2,#0
	mov x+3,#0
	lcall hex2bcd
	mov a,bcd+1
	lcall Display_mini_BCD
	mov r0, #0xc6
	mov a,bcd
	lcall Display_mini_BCD
	
	mov r0, #0xce
	mov a, reflowtime_p2
	lcall Display_mini_BCD
	mov Currpro_reflowtime, a
	ljmp profile_return
	
display_p3_data:	
	mov r0, #0x84
	mov x,soaktemp_p3
	mov x+1,soaktemp_p3+1
	mov x+2,#0
	mov x+3,#0
	lcall hex2bcd
	mov a,bcd+1
	lcall Display_mini_BCD
	mov r0, #0x86
	mov a,bcd
	lcall Display_mini_BCD
	
	mov r0, #0x8e
	mov a, soaktime_p3
	lcall Display_mini_BCD
	mov Currpro_soaktime, a
	
	mov r0, #0xc4
	mov x,reflowtemp_p3
	mov x+1,reflowtemp_p3+1
	mov x+2,#0
	mov x+3,#0
	lcall hex2bcd
	mov a,bcd+1
	lcall Display_mini_BCD
	mov r0, #0xc6
	mov a,bcd
	lcall Display_mini_BCD
	
	mov r0, #0xce
	mov a, reflowtime_p3
	lcall Display_mini_BCD
	mov Currpro_reflowtime, a
	ljmp profile_return

    
