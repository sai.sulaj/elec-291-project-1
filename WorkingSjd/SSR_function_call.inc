; Idea:
; Read our current temperature
; With some function that corresponds to our current state and profile
; retreive the desired temperature
; and infinitely loop via comparison

MainSSRProgram:	
; via function we're going to store into return_for_ssr the desired value 
MainSSRLoop:
	; function call here to get desired temp/value into accumalator
	mov x, Curr_temp
	mov x+1, Curr_temp+1
	mov x+2, #0
	mov x+3, #0
	mov y, return_for_ssr
	mov y+1, return_for_ssr+1
	mov y+2, #0
	mov y+3, #0
	lcall x_lt_y ; variable mf = 1 if x < y
	jb mf, less_than
	setb power_pin
	ret

less_than:
	clr power_pin
	ret
   

