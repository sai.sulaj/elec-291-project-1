// Approximate index of sounds in file 'wav_sound.wav'
code const unsigned long int wav_index[]={
    0x00002d, // 0 
    0x002568, // 1 
    0x006341, // 2 
    0x00a886, // 3 
    0x00eb7e, // 4 
    0x0133d7, // 5 
    0x01837c, // 6 
    0x01c450
};

// One 0-1, 
Computer_Sender.exe -COM7 -P0x00002d,16000
// Two, 1-2,
Computer_Sender.exe -COM7 -P0x002568,16000
// Three 2-3, 
Computer_Sender.exe -COM7 -P0x006341,16000
// Four 3-4, 
Computer_Sender.exe -COM7 -P0x00a886,16000
// Five 4-5,
Computer_Sender.exe -COM7 -P0x00eb7e,16000
// Six 5-6, 
Computer_Sender.exe -COM7 -P0x0133d7,16000
// // Approximate index of sounds in file 'wav_sound.wav'
Computer_Sender.exe -COM7 -P0x01837c,16000
// zero one two three four five six seven eight nine ten 